// HAPI I PARE: 
// Bejme nje liste te te gjitha kartave dhe i stokojme ne nje variable.
const cards = document.querySelectorAll(" .memory-card");

// HAPI I PESTE:
// Kur shtypim nje karte, duhet te dijme nese eshte karta e pare apo karta e dyte qe po e shtypim.
// Ne menyre qe te vazhdojme logjiken e lojes, deklarojme vleren e kartes qe eshte shtypur ne FALSE.
// Pastaj deklarojme dy variable te tjera, qe jane Karta e pare dhe Karta e dyte.
let hasFlippedCard = false;
let lockBoard = false;
let firstCard, secondCard;





// HAPI I TRETE:
// Deklarojme funksionin FLIPCARD, pastaj shikojme me nje console.log se cfare ka brenda konteksit te THIS. dhe pashe 
// qe brenda tij kishim klasen Memory-card. Keshtu qe ne kte kontekst, elementi THIS reprezanton elementin qe e shkrepi Eventin.
function flipCard() {
    if(lockBoard) return;
    if (this === firstCard) return;

// HAPI I KATERT:
// Cfare dua te bej ketu eshte : te kem akses te klasa Memory-card dhe ta bej TOGGLE, toggle do te thote: Nese klasa eshte aty,remove it
// Nese nuk eshte ADD it, apo shtoje.


// HAPI I GJASHTE:  
// Pastaj Toggle e ndrrojme me ADD. Dhe shtojme nje kondicion IF, nese Karta qe eshte e shtypur eshte e barabarte me Fasle!
// Kjo do te thote se eshte hera e pare qe po e shtypim nje karte. Klikimi i pare.
// Pastaj do e vendosim hasFlippedCard ne TRUE, dhe karten e pare e barabarte me THIS. 
// Pastaj shtojme dhe nje ELSE te cilen e heqim pastaj, ELSE: nese hasFlipedCard apo karta qe eshte e shtypur per momentin
// eshte e barabarte me false , kjo do te thote qe po shtypim karten e dyte. sepse !hasFlippedCard ne argument te IF.
    this.classList.add('flip');

    if (!hasFlippedCard){
        hasFlippedCard = true;
        firstCard = this;

        return;
    } 
       
        secondCard = this;

        checkForMatch();
}


function checkForMatch(){
    let isMatch = firstCard.dataset.framework === 
                  secondCard.dataset.framework;

    isMatch ? disableCards() : unFlipCards();
}


function disableCards(){
    firstCard.removeEventListener('click', flipCard);
    secondCard.removeEventListener('click', flipCard);

    const fliped = document.querySelectorAll(" .flip");


    if(fliped.length == 20) {

    setTimeout(() => { 
        $('#modal').modal('show')
    }, 1000); 
    };
    


    resetBoard();

}

document.getElementById('restart-game').addEventListener('click', function(){
    $('#modal').modal('hide');
    cards.forEach(card => {
        card.classList.remove('flip');
        card.addEventListener("click", flipCard)

});
shuffle();

});


function unFlipCards(){
    lockBoard = true;

    setTimeout(() => { 
        firstCard.classList.remove('flip');
        secondCard.classList.remove('flip');


       resetBoard();
    }, 1500); 

}


function resetBoard(){
    [hasFlippedCard, lockBoard] = [false, false];
    [firstCard, secondCard] = [null, null];
    /* hasFlippedCard = false;
    lockBoard = false;
    firstCard = null;
    secondCard = null; */
}

function shuffle(){
    cards.forEach(card => {
        let randomPos = Math.floor(Math.random() * 12);
        card.style.order = randomPos;
      });

}
shuffle();

//HAPI I DYTE: Ne listen qe e krijuam ne fillim tek variabla CARDS, do kalojme neper te gjitha kartat (loop) dhe te secila prej tyre
// do vendosim nje EVENT LISTENER, nje event me klikim, qe sa here te shtypim do ekzekutohet nje funksion qe e kam quajtur FLIPCARD.
cards.forEach(card => card.addEventListener("click", flipCard));


